//
//  feedInterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by jatin verma on 2019-10-31.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class feedInterfaceController: WKInterfaceController, WCSessionDelegate  {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    var hunger:Int  = 0 .hashValue
    
    @IBAction func FeedButton() {
        
      HungerIncrease()
    
        
    }
    
    func  HungerIncrease()
    {
        self.hunger = self.hunger + 1
        
        
    }
    @IBOutlet var hungerLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
