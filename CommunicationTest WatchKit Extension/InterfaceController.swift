//
//  InterfaceController.swift
//  CommunicationTest WatchKit Extension
//
//  Created by Parrot on 2019-10-26.
//  Copyright © 2019 Parrot. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate {
    
       var countdown = 0
       var myTimer: Timer? = nil
    var seconds = 120
    var timer = Timer()
    var isTimerRunning = false
    // MARK: Outlets
    // ---------------------
    @IBOutlet var messageLabel: WKInterfaceLabel!
   
    @IBOutlet var stateLabel: WKInterfaceLabel!
    @IBOutlet var hungerLabel: WKInterfaceLabel!
    @IBOutlet var Hplabel: WKInterfaceLabel!
    // Imageview for the pokemon
    @IBOutlet var pokemonImageView: WKInterfaceImage!
    // Label for Pokemon name (Albert is hungry)
    @IBOutlet var nameLabel: WKInterfaceLabel!
    // Label for other messages (HP:100, Hunger:0)
    @IBOutlet var outputLabel: WKInterfaceLabel!
    
    @IBOutlet var counterLabel: WKInterfaceLabel!
    
     func viewDidAppear(_ animated: Bool) {
        countdown = 100
        myTimer = Timer(timeInterval: 1.0, target: self, selector:"countDownTick", userInfo: nil, repeats: true)
        counterLabel.setText("\(countdown)")
    }

    func countDownTick() {
        countdown = countdown - 1

        if (countdown == 0) {
           myTimer!.invalidate()
           myTimer=nil
        }

        counterLabel.setText;"\(countdown)"
    }
    // MARK: Delegate functions
    // ---------------------

    // Default function, required by the WCSessionDelegate on the Watch side
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        //@TODO
    }
    
    // 3. Get messages from PHONE
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
         print("WATCH: Got message from Phone")
               // Message from phone comes in this format: ["course":"MADT"]
               let messageBody1 = message["pokemon"] as! String
         let messageBody2 = message["HP"] as! String
         let messageBody3 = message["hunger" ] as! String
         let messageBody4 = message["state"] as! String
               messageLabel.setText(messageBody1)
         Hplabel.setText(messageBody2)
             hungerLabel.setText(messageBody3)
        stateLabel.setText(messageBody4)
    }
    


    
    // MARK: WatchKit Interface Controller Functions
    // ----------------------------------
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        // 1. Check if teh watch supports sessions
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
        }
        
        
    }
    @IBOutlet var timerLabel: WKInterfaceLabel!
    func runTimer() {
         timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(InterfaceController.updateTimer)), userInfo: nil, repeats: true)
    }
    @objc func updateTimer() {
        seconds = seconds - 1     //This will decrement(count down)the seconds.
        self.timerLabel.setText("\(seconds)")
        //This will update the label.
    }
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
    }
    @IBAction func startTimerButton() {
    
        self.runTimer()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    // MARK: Actions
    // ---------------------
    
  
    
    
    // MARK: Functions for Pokemon Parenting
    @IBAction func nameButtonPressed() {
       
    }

    @IBAction func startButtonPressed() {
        print("Start button pressed")
    }
    
    @IBAction func feedButtonPressed() {
        print("Feed button pressed")
    }
    
    @IBAction func hibernateButtonPressed() {
        print("Hibernate button pressed")
    }
    
}
